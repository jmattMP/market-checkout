package com.jmattMP.market.checkout.bootstrap.configuration;

import com.jmattMP.market.checkout.adapter.persistence.PersistenceConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        //Application core
        ApplicationServiceConfiguration.class,

        //Adapters

        //DRIVING
        //Web REST
        //Web UI

        //DRIVEN
        PersistenceConfiguration.class

        //Infrastructure
})
public class AppConfig {


}


/*
@Import({


//Application service
        ApplicationServiceConfiguration.class,

//Web Rest
        SetupEmbeddedTomcatWithTempWorkDir.class,
        SetupTomcatSessionTrackingInCookie.class,
        SetupSpringDispatcherServletForRootPath.class,
        SetupWebCommonsMultipartResolver.class,
        SetupEnforceUtf8EncodingFilter.class,
        SetupSpringTrimmingWebDataBinderControllerAdvice.class,
        SetupExceptionHandlerOnlyHandlerExceptionResolver.class,
        SetupVideobrainAccessLogging.class,
        SetupVideobrainWebErrorHandling.class,
        SetupRedirectToHttpsFilter.class,
        SetupGeneralConfiguredPasswordProtectionFilter.class,

//WEB UI
        SetupConfigAdjustedApplicationUrl.class, SetupWebResourcesPubliclyExposed.class,
        SetupGlobalUiCurrentRequestNotificationMessagesAttribute.class,
        SetupGlobalUiAttributesService.class,
        SetupGlobalUiApplicationUrlAttribute.class,
        SetupDefaultActionTokenController.class,
        SetupGlobalUiVideobrainDateFormatterAttribute.class,
        SetupGlobalUiTextFormatterAttribute.class,
        SetupGlobalUiVideobrainFrontendVersionAttribute.class,

// Infra
        SetupConfigProviderFromPropertiesFile.class,
        SetupKurentoRecordingConfiguration.class,
        SetupVideobrainDraftProviderIntegrations.class,

// Storage
        SetupVideobrainJooqPersistenceServices.class,
        SetupVideobrainInMemoryPersistenceServices.class,
        SetupFilesystemMediaServerRecordingSource.class,

// Features / Business
        SetupVideobrainBusinessServices.class,
        SetupExampleData.class,
        SetupHomePage.class,
        SetupCompanyWebServices.class,
        SetupRecruitmentWebServices.class})
 */