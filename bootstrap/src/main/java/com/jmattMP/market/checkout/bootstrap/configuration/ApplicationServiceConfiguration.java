package com.jmattMP.market.checkout.bootstrap.configuration;

import com.jmattMP.market.checkout.application.ApplicationCommonConfiguration;
import com.jmattMP.market.checkout.application.port.in.ProductCrud;
import com.jmattMP.market.checkout.application.port.out.ProductRepository;
import com.jmattMP.market.checkout.application.service.ProductCrudService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({ApplicationCommonConfiguration.class})
public class ApplicationServiceConfiguration {

    @Bean
    public ProductCrud productProcessingUseCase(ProductRepository productRepository) {
        return new ProductCrudService(productRepository);
    }
}
