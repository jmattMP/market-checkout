package com.jmattMP.market.checkout.adapter.persistence.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "product")
public class ProductJpa implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "product_id")
    private long productId;
    @Column(name = "product_barcode", unique = false, nullable = false)
    private long productBarcode;
    @Column(name = "product_name")
    private String productName;
    @Column(name = "product_price")
    private BigDecimal unitPrice;
    @Column(name = "producer_info")
    private String producerData;
    @Column(name = "expiration_date")
    private LocalDate expirationDate;
    @Column(name = "product_type")
    private String productType;
    // @ElementCollection
    @Column(name = "product_tags")
    private String productTags;
    @Column(name = "product_details")
    private String productDetails;

    public ProductJpa(long productBarcode, String productName, BigDecimal unitPrice, String producerData, LocalDate expirationDate, String productType, String productTags, String productDetails) {
        this.productBarcode = productBarcode;
        this.productName = productName;
        this.unitPrice = unitPrice;
        this.producerData = producerData;
        this.expirationDate = expirationDate;
        this.productType = productType;
        this.productTags = productTags;
        this.productDetails = productDetails;
    }

    @Override
    public String toString() {
        return String.format(
                "Product[id=%d, firstName='%s', lastName='%s']",
                productId, productName, producerData);
    }
}
