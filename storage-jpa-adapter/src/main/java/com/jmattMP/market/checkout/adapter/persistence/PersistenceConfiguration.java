package com.jmattMP.market.checkout.adapter.persistence;

import com.jmattMP.market.checkout.adapter.persistence.configuration.DataSourceConfiguration;
import com.jmattMP.market.checkout.adapter.persistence.repository.ProductJpaRepository;
import com.jmattMP.market.checkout.adapter.persistence.repository.ProductRepositoryImpl;
import com.jmattMP.market.checkout.application.port.out.ProductRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.persistence.EntityManager;

@Configuration
@Import(DataSourceConfiguration.class)
@EnableJpaRepositories(basePackages = "com.jmattMP.market.checkout.adapter.persistence.repository")
public class PersistenceConfiguration {

    @Bean
    public ProductRepository productRepository(ProductJpaRepository productJpaRepository, EntityManager entityManager) {
        return new ProductRepositoryImpl(productJpaRepository, entityManager);
    }
}
