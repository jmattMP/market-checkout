package com.jmattMP.market.checkout.adapter.persistence.repository;

import com.jmattMP.market.checkout.adapter.persistence.entity.ProductJpa;
import com.jmattMP.market.checkout.adapter.persistence.mapper.ProductJpaMapper;
import com.jmattMP.market.checkout.application.dto.ProductState;
import com.jmattMP.market.checkout.application.port.out.ProductRepository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.Collection;

public class ProductRepositoryImpl implements ProductRepository {

    private final ProductJpaRepository productJpaRepository;
    private final EntityManager entityManager;

    public ProductRepositoryImpl(ProductJpaRepository productJpaRepository, EntityManager entityManager) {
        this.productJpaRepository = productJpaRepository;
        this.entityManager = entityManager;
    }

    @Override
    public Collection<ProductState> getAvailableProductsByNameAndProductType(String name, String productType) {
        return ProductJpaMapper.productsJpaToProductsState(
                productJpaRepository.getAvailableProducts(name, productType));
    }

    @Override
    public Collection<ProductState> getAvailableProductsByName(String name) {
        return ProductJpaMapper.productsJpaToProductsState(
                productJpaRepository.findByProductNameOrProducerDataContainsIgnoreCase(name, name));
    }

    @Override
    public Collection<ProductState> getAvailableProductsByProductType(String productType) {
        return ProductJpaMapper.productsJpaToProductsState(
                productJpaRepository.findByProductType(productType));
    }

    @Override
    public Collection<ProductState> getAllProducts() {
        return ProductJpaMapper.productsJpaToProductsState(productJpaRepository.findAll());
    }

    @Override
    @Transactional
    public void addNewProduct(ProductState product) {
        ProductJpa productJpa = ProductJpaMapper.productStateToProductJpa(product);
        entityManager.persist(productJpa);
    }

    @Override
    public ProductState getProductByBarcode(long barcode) {
        return ProductJpaMapper.productJpaToProductState(
                productJpaRepository.findByProductBarcode(barcode));
    }

    @Override
    @Transactional
    public ProductState removeProduct(long productBarcode) {
        ProductJpa product = productJpaRepository.findByProductBarcode(productBarcode);
        productJpaRepository.delete(product);
        return ProductJpaMapper.productJpaToProductState(product);
    }
}
