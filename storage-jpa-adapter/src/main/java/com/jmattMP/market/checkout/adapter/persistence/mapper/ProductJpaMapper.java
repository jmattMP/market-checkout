package com.jmattMP.market.checkout.adapter.persistence.mapper;


import com.jmattMP.market.checkout.adapter.persistence.entity.ProductJpa;
import com.jmattMP.market.checkout.application.dto.ProductState;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class ProductJpaMapper {

    //ProductJpa <-> ProductState
    public static ProductJpa productStateToProductJpa(ProductState productState) {
        return new ProductJpa(productState.getProductBarcode(),
                productState.getProductName(),
                productState.getUnitPrice(),
                productState.getProducerData(),
                productState.getExpirationDate(),
                productState.getProductType(),
                productState.getProductTags().stream().collect(Collectors.joining(",")),
                productState.getProductDetails());
    }

    public static ProductState productJpaToProductState(ProductJpa productJpa) {
        return new ProductState(productJpa.getProductBarcode(),
                productJpa.getProductName(),
                productJpa.getUnitPrice(),
                productJpa.getProducerData(),
                productJpa.getExpirationDate(),
                productJpa.getProductType(),
                Stream.of(productJpa.getProductTags().split(",", -1))
                        .collect(Collectors.toList()),
                productJpa.getProductDetails());
    }

    public static Collection<ProductJpa> productsStateToProductsJpa(Collection<ProductState> productsState) {
        return productsState.stream().map(ProductJpaMapper::productStateToProductJpa).collect(toList());
    }

    public static Collection<ProductState> productsJpaToProductsState(Collection<ProductJpa> productJpa) {
        return productJpa.stream().map(ProductJpaMapper::productJpaToProductState).collect(toList());
    }

}
