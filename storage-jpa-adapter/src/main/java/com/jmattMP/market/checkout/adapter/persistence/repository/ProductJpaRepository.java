package com.jmattMP.market.checkout.adapter.persistence.repository;

import com.jmattMP.market.checkout.adapter.persistence.entity.ProductJpa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ProductJpaRepository extends JpaRepository<ProductJpa, Long> {

    @Query("FROM ProductJpa p WHERE p.productName = :term AND p.productType = :type")
    Collection<ProductJpa> getAvailableProducts(@Param("term") String term, @Param("type") String productType);

    Collection<ProductJpa> findByProductNameOrProducerDataContainsIgnoreCase(String productName, String producerData);

    Collection<ProductJpa> findByProductType(String productType);

    ProductJpa findByProductBarcode(long barcode);
}
