CREATE TABLE product(
product_id INT UNSIGNED NOT NULL AUTO_INCREMENT,
PRIMARY KEY (product_id),
product_barcode INT(100),
product_name  varchar(100),
product_price DECIMAL(6,4),
producer_info  varchar(100),
expiration_date  date,
product_type varchar(100),
product_tags  varchar(100),
product_details  varchar(100)
);

