package com.jmattMP.market.checkout.adapter.ws.v1.json;

import lombok.Getter;

@Getter
public class MailReceiptRequest {
    private String email;
    private long orderId;

    public MailReceiptRequest(String email, long orderId) {
        this.email = email;
        this.orderId = orderId;
    }
}
