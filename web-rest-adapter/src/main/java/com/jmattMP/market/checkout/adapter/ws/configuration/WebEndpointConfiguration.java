package com.jmattMP.market.checkout.adapter.ws.configuration;

import com.jmattMP.market.checkout.adapter.ws.common.CommentController;
import com.jmattMP.market.checkout.adapter.ws.common.HealthCheckEndpoint;
import com.jmattMP.market.checkout.adapter.ws.common.RestErrorHandler;
import com.jmattMP.market.checkout.adapter.ws.v1.configuration.EndpointsVer1Configuration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = EndpointsVer1Configuration.class)
public class WebEndpointConfiguration {

    /**
     * Default error Handler
     */
    @Bean
    public RestErrorHandler restErrorHandler(MessageSource messageSource) {
        return new RestErrorHandler(messageSource);
    }


    @Bean
    public HealthCheckEndpoint healthCheckEndpoint() {
        return new HealthCheckEndpoint();
    }

    @Bean
    public CommentController commentController() {
        return new CommentController();
    }


}
