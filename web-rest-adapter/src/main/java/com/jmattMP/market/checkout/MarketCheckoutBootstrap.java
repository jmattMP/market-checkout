package com.jmattMP.market.checkout;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.jmattMP.market.checkout.adapter.ws.configuration")
public class MarketCheckoutBootstrap {

    public static void main(String[] args) {
        SpringApplication.run(MarketCheckoutBootstrap.class, args);
    }

}
