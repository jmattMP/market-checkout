package com.jmattMP.market.checkout.adapter.ws.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.collect.Lists.newArrayList;
import static springfox.documentation.builders.PathSelectors.any;

@Configuration
@EnableSwagger2
public class OpenApiConfiguration {

    @Bean
    public Docket apiDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfoV1())
                .groupName("v1")
                .select()
                .paths(PathSelectors.regex("\\/(ws/v1.+)"))
                .build();
    }


    private ApiInfo apiInfoV1() {
        return new ApiInfoBuilder()
                .title("Open API Documentation")
                .description("Open API documentation of accessible endpoints")
                .contact(new Contact("Mateusz P",
                        "http://none.pl",
                        "email@here.pl"))
                .license("License name here")
                .licenseUrl("URL to license")
                .version("1.0.1")
                .build();
    }

    @Bean
    public Docket apiV2() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("v2")
                .apiInfo(apiInfoV2())
                .select()
                .paths(PathSelectors.regex("\\/(ws/v2.+)"))
                .build();
    }

    private ApiInfo apiInfoV2() {
        return new ApiInfoBuilder()
                .title("Open API Documentation version 2")
                .description("Open API documentation of accessible endpoints")
                .contact(new Contact("Mateusz P",
                        "http://none.pl",
                        "email@here.pl"))
                .license("License name here")
                .licenseUrl("URL to license")
                .version("2.0.1")
                .build();
    }

    public Docket exampleDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("open-api")
                .apiInfo(apiInfoV1())
                .select()
                // .apis(RequestHandlerSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.jmattMP.market.checkout.ws"))
                //  .paths(PathSelectors.regex("/spanish-greeting.*"))
                .paths(any())
                .build()
                .globalResponseMessage(RequestMethod.GET,
                        newArrayList(new ResponseMessageBuilder()
                                .code(500)
                                .message("500 message")
                                .responseModel(new ModelRef("Error"))
                                .build()));
    }
}

