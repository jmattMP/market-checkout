package com.jmattMP.market.checkout.adapter.ws.v1.product;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public class ProductResponse {
    private long productBarcode;
    private String productName;
    private BigDecimal unitPrice;
    private String producerData;
    private LocalDate expirationDate;
    private String productType;
    private List<String> productTags;
    private String productDetails;
}
