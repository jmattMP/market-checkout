package com.jmattMP.market.checkout.adapter.ws.v1.mapper;

import com.jmattMP.market.checkout.domain.Money;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MoneyMapper {

    public BigDecimal asBigDecimal(Money money) {
        return money.getCurrentAmountAsBigDecimal();
    }

    public Money asMoney(BigDecimal bigDecimal) {
        return Money.inPLNFrom(bigDecimal.doubleValue());
    }

    public String asString(Date date) {
        return date != null ? new SimpleDateFormat("yyyy-MM-dd")
                .format(date) : null;
    }

    public Date asDate(String date) {
        try {
            return date != null ? new SimpleDateFormat("yyyy-MM-dd")
                    .parse(date) : null;
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
}
