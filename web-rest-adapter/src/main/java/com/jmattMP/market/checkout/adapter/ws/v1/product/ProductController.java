package com.jmattMP.market.checkout.adapter.ws.v1.product;

import com.jmattMP.market.checkout.adapter.ws.v1.mapper.ProductWebServiceMapper;
import com.jmattMP.market.checkout.application.dto.ProductAPI;
import com.jmattMP.market.checkout.application.port.in.ProductCrud;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@Api(tags = "product")
@RestController
@RequestMapping("ws/v1/products")
@AllArgsConstructor
public class ProductController {

    private final ProductCrud productCrud;

    @ApiOperation(value = "Return available products")
    @GetMapping
    public Collection<ProductResponse> getAvailableProducts(@RequestParam(required = false) String name,
                                                            @RequestParam(required = false) String productType) {
        Collection<ProductAPI> productsAPI = productCrud.getAvailableProducts(name, productType);
        return ProductWebServiceMapper.productsApiToProductsResponse(productsAPI);
    }

    @ApiOperation(value = "Return available product by barcode")
    @GetMapping("{productBarcode}")
    public ProductResponse getProductByBarcode(@RequestParam long productBarcode) {
        return ProductWebServiceMapper.productApiToProductResponse(productCrud.getProductByBarcode(productBarcode));
    }

    @ApiOperation(value = "Add new product")
    @PostMapping("admin/add")
    public void addProduct(@Valid ProductRequest productRequest) {
        ProductAPI productAPI = ProductWebServiceMapper.productRequestToProductAPI(productRequest);
        productCrud.addNewProduct(productAPI);
    }

    @ApiOperation(value = "Remove existed product from market")
    @DeleteMapping("admin/remove/{productBarcode}")
    public ResponseEntity<ProductResponse> removeProduct(@RequestParam long productBarcode) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(ProductWebServiceMapper.productApiToProductResponse(
                        productCrud.removeProduct(productBarcode)));
    }
}

