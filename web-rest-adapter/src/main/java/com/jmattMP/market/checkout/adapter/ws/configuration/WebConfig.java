package com.jmattMP.market.checkout.adapter.ws.configuration;

import com.jmattMP.market.checkout.bootstrap.configuration.AppConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(AppConfig.class)
public class WebConfig {
}
