package com.jmattMP.market.checkout.adapter.ws.v1.payment;

import com.jmattMP.market.checkout.adapter.ws.v1.json.PaymentMethodStatus;
import com.jmattMP.market.checkout.adapter.ws.v1.json.PaymentRequest;
import com.jmattMP.market.checkout.domain.payment.PaymentMethod;
import com.jmattMP.market.checkout.service.dto.PaymentMethodResponse;
import com.jmattMP.market.checkout.service.dto.PaymentProgress;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Api(tags = "payment")
@RestController
@RequestMapping("ws/v1/payment")
public class PaymentRestController {

    @ApiOperation(value = "Return available method payments")
    @GetMapping
    public PaymentMethodResponse getAvailablePaymentMethods() {
        //todo do we need here pagination?
        //todo payment methods should be configurable!!
        return new PaymentMethodResponse(List.of(PaymentMethod.CASH, PaymentMethod.CREDIT_CARD));
    }

    @ApiOperation(value = "Pay for order")
    @PostMapping("{orderId}")
    public PaymentMethodStatus payOrder(@PathVariable long orderId,
                                        @RequestBody @Valid PaymentRequest paymentRequest) {
        return new PaymentMethodStatus(123l, 222l, PaymentProgress.ACCEPTED);
    }


}
