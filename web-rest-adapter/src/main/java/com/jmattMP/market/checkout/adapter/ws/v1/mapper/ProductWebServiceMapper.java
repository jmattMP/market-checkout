package com.jmattMP.market.checkout.adapter.ws.v1.mapper;

import com.jmattMP.market.checkout.adapter.ws.v1.product.ProductRequest;
import com.jmattMP.market.checkout.adapter.ws.v1.product.ProductResponse;
import com.jmattMP.market.checkout.application.dto.ProductAPI;

import java.util.Collection;

import static java.util.stream.Collectors.toList;

public class ProductWebServiceMapper {

    // ProductAPI <-> Product Response
    public static ProductAPI productResponseToProductApi(ProductResponse productState) {
        return new ProductAPI(productState.getProductBarcode(),
                productState.getProductName(),
                productState.getUnitPrice(),
                productState.getProducerData(),
                productState.getExpirationDate(),
                productState.getProductType(),
                productState.getProductTags(),
                productState.getProductDetails());
    }

    public static Collection<ProductAPI> productResponseToProductApi(Collection<ProductResponse> productsResponse) {
        return productsResponse.stream().map(ProductWebServiceMapper::productResponseToProductApi).collect(toList());
    }

    public static ProductResponse productApiToProductResponse(ProductAPI productApi) {
        return new ProductResponse(productApi.getProductBarcode(),
                productApi.getProductName(),
                productApi.getUnitPrice(),
                productApi.getProducerData(),
                productApi.getExpirationDate(),
                productApi.getProductType(),
                productApi.getProductTags(),
                productApi.getProductDetails());
    }

    public static Collection<ProductResponse> productsApiToProductsResponse(Collection<ProductAPI> productsResponse) {
        return productsResponse.stream().map(ProductWebServiceMapper::productApiToProductResponse).collect(toList());
    }

    // Product Request -> ProductApi
    public static ProductAPI productRequestToProductAPI(ProductRequest productRequest) {
        return new ProductAPI(productRequest.getProductBarcode(),
                productRequest.getProductName(),
                productRequest.getUnitPrice(),
                productRequest.getProducerData(),
                productRequest.getExpirationDate(),
                productRequest.getProductType(),
                productRequest.getProductTags(),
                productRequest.getProductDetails());
    }

    public static Collection<ProductAPI> productsRequestToProductsAPI(Collection<ProductRequest> productsRequest) {
        return productsRequest.stream().map(ProductWebServiceMapper::productRequestToProductAPI).collect(toList());
    }
}


