package com.jmattMP.market.checkout.adapter.ws.v1.receipt;

import com.jmattMP.market.checkout.adapter.ws.v1.json.MailReceiptRequest;
import com.jmattMP.market.checkout.service.dto.OrderItem;
import com.jmattMP.market.checkout.service.dto.ReceiptDetails;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags = "receipt")
@RestController
@RequestMapping("ws/v1/receipt")
public class ReceiptRestController {

    @ApiOperation(value = "Return receipt for order")
    @GetMapping("{orderId}")
    public ReceiptDetails getReceipt(@PathVariable long orderId) {
        return new ReceiptDetails(List.of(new OrderItem(12, 124)), 124);
    }

    @ApiOperation(value = "Send receipt via email")
    @PostMapping
    public void sendMailWithReceipt(@RequestBody MailReceiptRequest mailReceiptRequest) {
        //todo return 204 or 4xxx
    }
}
