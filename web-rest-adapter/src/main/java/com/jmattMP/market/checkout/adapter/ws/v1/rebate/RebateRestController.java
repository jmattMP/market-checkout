package com.jmattMP.market.checkout.adapter.ws.v1.rebate;

import com.jmattMP.market.checkout.service.dto.RebateDetails;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "rebate")
@RestController
@RequestMapping("ws/v1/rebate")
public class RebateRestController {

    @ApiOperation(value = "Rebate for order for order")
    @GetMapping("{orderId}")
    public RebateDetails calculateRebate(@PathVariable long orderId) {
        //todo use Money instead
        return new RebateDetails(120, 20);
    }

    /**
     * Optional endpoints
     */
    //POST introduce new Rebate
    //DELETE delete current Rebate
}
