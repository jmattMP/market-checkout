package com.jmattMP.market.checkout.adapter.ws.common;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "utils")
@RestController
@RequestMapping("ws/v2/utils")
public class HealthCheckEndpoint {
    private Logger logger = LoggerFactory.getLogger(HealthCheckEndpoint.class);

    @ApiOperation(value = "Healthcheck point", nickname = "helthcheck")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = HealthCheckEndpoint.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @GetMapping("healthCheck")
    public String healthUp() {
        logger.trace("A TRACE Message");
        logger.debug("A DEBUG Message");
        logger.info("An INFO Message");
        logger.warn("A WARN Message");
        logger.error("An ERROR Message");
        return "Howdy! Check out the Logs to see the output...";
    }
}
