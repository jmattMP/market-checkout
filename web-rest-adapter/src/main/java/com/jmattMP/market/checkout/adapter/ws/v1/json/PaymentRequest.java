package com.jmattMP.market.checkout.adapter.ws.v1.json;

import com.jmattMP.market.checkout.domain.payment.PaymentMethod;
import com.jmattMP.market.checkout.service.dto.PaymentDetails;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class PaymentRequest {
    @NotNull
    private PaymentMethod choosenPaymentMethod;
    @NotNull
    private PaymentDetails paymentDetails;

    public PaymentRequest(PaymentMethod choosenPaymentMethod, PaymentDetails paymentDetails) {
        this.choosenPaymentMethod = choosenPaymentMethod;
        this.paymentDetails = paymentDetails;
    }
}
