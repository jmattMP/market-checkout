package com.jmattMP.market.checkout.adapter.ws.v1.json;

import com.jmattMP.market.checkout.service.dto.PaymentProgress;
import lombok.Getter;

@Getter
public class PaymentMethodStatus {
    private long paymentId;
    private long orderId;
    private PaymentProgress paymentProgress;

    public PaymentMethodStatus(long paymentId, long orderId, PaymentProgress paymentProgress) {
        this.paymentId = paymentId;
        this.orderId = orderId;
        this.paymentProgress = paymentProgress;
    }
}
