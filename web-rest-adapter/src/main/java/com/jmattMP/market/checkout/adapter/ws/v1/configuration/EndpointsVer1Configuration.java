package com.jmattMP.market.checkout.adapter.ws.v1.configuration;

import com.jmattMP.market.checkout.adapter.ws.v1.order.OrderRestController;
import com.jmattMP.market.checkout.adapter.ws.v1.payment.PaymentRestController;
import com.jmattMP.market.checkout.adapter.ws.v1.product.ProductController;
import com.jmattMP.market.checkout.adapter.ws.v1.rebate.RebateRestController;
import com.jmattMP.market.checkout.adapter.ws.v1.receipt.ReceiptRestController;
import com.jmattMP.market.checkout.application.port.in.ProductCrud;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EndpointsVer1Configuration {

    @Bean
    public OrderRestController orderRestController() {
        return new OrderRestController();
    }

    @Bean
    public PaymentRestController paymentRestController() {
        return new PaymentRestController();
    }

    @Bean
    public RebateRestController rebateRestController() {
        return new RebateRestController();
    }

    @Bean
    public ReceiptRestController receiptRestController() {
        return new ReceiptRestController();
    }

    @Bean
    public ProductController productController(ProductCrud productCrud) {
        return new ProductController(productCrud);
    }
}
