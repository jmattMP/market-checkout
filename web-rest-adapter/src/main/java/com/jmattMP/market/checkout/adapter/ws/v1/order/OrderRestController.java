package com.jmattMP.market.checkout.adapter.ws.v1.order;

import com.jmattMP.market.checkout.service.dto.OrderItem;
import com.jmattMP.market.checkout.service.dto.OrderRepresentation;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("ws/v1/orders")
@Api(tags = "order")
public class OrderRestController {

    //public final OrderProcessingUseCase orderUseCase;

    @ApiOperation(value = "Return order item information")
    @GetMapping("orderItem/{orderItemId}")
    public OrderItem getOrderItem(@PathVariable long orderItemId) {
        return new OrderItem(12345, 304);
    }

    @ApiOperation(value = "Add order item to basket")
    @PutMapping("{orderId}/orderItem/{orderItemId}")
    public OrderRepresentation addOrderItem(@PathVariable long orderId,
                                            @PathVariable long orderItemId) {
        OrderItem orderItem1 = new OrderItem(123, 100);
        OrderItem orderItem2 = new OrderItem(124, 100);
        OrderItem orderItem3 = new OrderItem(125, 100);
        OrderItem orderItem4 = new OrderItem(126, 100);
        List<OrderItem> orderItems = List.of(orderItem1, orderItem2, orderItem3, orderItem4);
        return new OrderRepresentation(1231, orderItems, 123);
    }

    @ApiOperation(value = "Remove order item from basket")
    @DeleteMapping("{orderId}/orderItem/{orderItemId}")
    public OrderRepresentation removeOrderItem(@PathVariable long orderId,
                                               @PathVariable long orderItemId) {
        OrderItem orderItem1 = new OrderItem(123, 100);
        OrderItem orderItem2 = new OrderItem(124, 100);
        OrderItem orderItem3 = new OrderItem(125, 100);
        List<OrderItem> orderItems = List.of(orderItem1, orderItem2, orderItem3);
        return new OrderRepresentation(1231, orderItems, 123);
    }

    @ApiOperation(value = "Creates new order")
    @PostMapping
    public OrderRepresentation createNewOrder() {
        return new OrderRepresentation(1231, new ArrayList<>(), 123);
    }

    @ApiOperation(value = "Return current order")
    @GetMapping("{orderId}")
    public OrderRepresentation getCurrentOrder(@PathVariable long orderId) {
        OrderItem orderItem1 = new OrderItem(123, 100);
        OrderItem orderItem2 = new OrderItem(124, 100);
        OrderItem orderItem3 = new OrderItem(125, 100);
        List<OrderItem> orderItems = List.of(orderItem1, orderItem2, orderItem3);
        return new OrderRepresentation(1231, orderItems, 123);
    }

    @ApiOperation(value = "Remove order")
    @DeleteMapping("{orderId}")
    public void cancelOrder(@PathVariable long orderId) {
        //success 204, error 4xx (not exist etc)
    }


}
