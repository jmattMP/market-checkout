package com.jmattMP.market.checkout.adapter.ws.v1.product;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Getter
public final class ProductRequest {
    private long productBarcode;
    private String productName;
    private BigDecimal unitPrice;
    private String producerData;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private LocalDate expirationDate;
    private String productType;
    private List<String> productTags;
    private String productDetails;
}


