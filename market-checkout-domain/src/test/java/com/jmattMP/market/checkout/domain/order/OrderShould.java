package com.jmattMP.market.checkout.domain.order;

import com.jmattMP.market.checkout.domain.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class OrderShould {
    private Order order;

    @BeforeEach
    void setUp() {
        order = Order.createNewOrder();
    }

    @Test
    void createNewOrderWithIdContainsDateOfCreation() {
        String localDate = LocalDate.now().toString();
        double priceForOrder = order.getTotalPrice().getCurrentAmount();
        assertThat(order.getOrderId()).startsWith(localDate);
        assertThat(order.getOrderLines().size()).isEqualTo(0);
        assertThat(priceForOrder).isEqualByComparingTo(0.0);
    }

    @Test
    void addOrderItem() {
        int numberOfOrderLinesAtTheBeginning = order.getOrderLines().size();
        order.addOrderItem(OrderLine.of(123l, 1, Money.inPLNFrom(12.0)));
        order.addOrderItem(OrderLine.of(123l, 1, Money.inPLNFrom(14.0)));
        assertThat(numberOfOrderLinesAtTheBeginning).isEqualTo(0);
        assertThat(order.getOrderLines().size()).isEqualTo(2);
    }

    @Test
    void removeOrderLine() {
    }

    @Test
    void getTotalPrice() {
    }

    @Test
    void getOrderId() {
    }

    @Test
    void getCreationDateTime() {
    }

    @Test
    void getOrderLines() {
    }
}