package com.jmattMP.market.checkout.domain;

import java.math.BigDecimal;
import java.util.Currency;

public final class Money {
    private final BigDecimal amount;
    private final Currency currency;

    private Money(BigDecimal amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public Money add(Money money) {
        BigDecimal currentAmount = getCurrentAmountAsBigDecimal().add(BigDecimal.valueOf(money.getCurrentAmount()));
        return new Money(currentAmount, currency);
    }

    public Money subtract(Money money) {
        BigDecimal currentAmount = getCurrentAmountAsBigDecimal().subtract(BigDecimal.valueOf(money.getCurrentAmount()));
        return new Money(currentAmount, currency);
    }

    public double getCurrentAmount() {
        return amount.doubleValue();
    }

    public BigDecimal getCurrentAmountAsBigDecimal() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public static Money from(double amount, Currency currency) {
        return new Money(BigDecimal.valueOf(amount), currency);
    }

    public static Money inEuroFrom(double amount) {
        return new Money(BigDecimal.valueOf(amount), Currency.getInstance("EUR"));
    }

    public static Money inPLNFrom(double amount) {
        return new Money(BigDecimal.valueOf(amount), Currency.getInstance("PLN"));
    }

    public static Money inEUR() {
        return new Money(BigDecimal.valueOf(0.0), Currency.getInstance("EUR"));
    }

    public static Money inPLN() {
        return new Money(BigDecimal.valueOf(0.0), Currency.getInstance("PLN"));
    }
}
