package com.jmattMP.market.checkout.domain.order;

public interface OrderRepository {
    Order createOrder(Order order);

    Order cancelOrder(Order order);

    Order submitOrder(Order order);
}
