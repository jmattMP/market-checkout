package com.jmattMP.market.checkout.domain.rebate;

import com.jmattMP.market.checkout.domain.common.DomainPolicy;

@DomainPolicy
public interface RebatePolicy {
    /**
     * calculates rebate for order based on specific rebate policy
     * net value
     */
    Rebate calculateRebate(long orderId, RebatePolicy rebatePolicy);
}



