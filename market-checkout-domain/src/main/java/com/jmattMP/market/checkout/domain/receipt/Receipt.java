package com.jmattMP.market.checkout.domain.receipt;

import com.jmattMP.market.checkout.domain.Money;
import com.jmattMP.market.checkout.domain.order.OrderElements;
import lombok.AllArgsConstructor;

import java.util.List;

//TODO MAYBE DTO -> DTO FOR PRESENTATION PURPOSE SHOULD BE IN PRESENTATION, DTO FOR GENERAL PURPOSE SHOULD BE IN APPLICATION SERVICE
@AllArgsConstructor
public class Receipt {
    private final long receiptId;
    private final long orderId;
    private final List<OrderElements> orderElements;
    private final Money totalPrice;
    private final ReceiptHeader receiptHeader; //dane sklepu - konfigurowalne
    private final ReceiptFooter receiptFooter; //dane podatkowe - konfigurowalne

    //todo czy header i footer should be here?


}
