package com.jmattMP.market.checkout.domain.order;

import com.jmattMP.market.checkout.domain.Money;
import com.jmattMP.market.checkout.domain.common.IdentityGenerator;
import lombok.Getter;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
public class Order {
    private final String orderId;
    private final LocalDateTime creationDateTime;
    private List<OrderLine> orderLines;
    private Money totalPrice;

    private Order() {
        orderLines = new ArrayList<>();
        totalPrice = Money.inPLN();
        creationDateTime = LocalDateTime.now();
        orderId = IdentityGenerator.generateIdentityBasedOnData();
    }

    public static Order createNewOrder() {
        return new Order();
    }

    public Order addOrderItem(OrderLine orderLine) {
        orderLines.add(orderLine);
        return this;
    }

    public Order removeOrderLine(OrderLine orderLine) {
        orderLines.remove(orderLine);
        return this;
    }

    private void calculateTotalPrice() {
        totalPrice = Money.inPLN();
        orderLines.forEach(orderLine -> totalPrice.add(orderLine.getSummaryPrice()));
    }

    public Money getTotalPrice() {
        calculateTotalPrice();
        return totalPrice;
    }
}
