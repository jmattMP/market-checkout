package com.jmattMP.market.checkout.domain.common;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface DomainRepository {
    /**
     * This interface is used as a marker to emphasize meaning of marked class/interface
     */
    String value() default "";
}
