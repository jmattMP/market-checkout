package com.jmattMP.market.checkout.domain.product;

import com.jmattMP.market.checkout.domain.Money;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.time.LocalDate;
import java.util.List;

@AllArgsConstructor
@Getter
public class Product {
    private long productBarcode;
    private String productName;
    private Money unitPrice;
    private String producerData;
    private LocalDate expirationDate;
    private String productType;
    private List<String> productTags;
    private String productDetails;
}
