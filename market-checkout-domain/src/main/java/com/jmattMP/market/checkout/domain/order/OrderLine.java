package com.jmattMP.market.checkout.domain.order;

import com.jmattMP.market.checkout.domain.Money;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor(staticName = "of")
@Getter
public class OrderLine {
    private final long productId;
    private final int elementsCount;
    private final Money summaryPrice;
}
