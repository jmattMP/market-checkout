package com.jmattMP.market.checkout.domain.common;

import java.time.LocalDate;
import java.util.UUID;

public class IdentityGenerator {
    public static String generateIdentityBasedOnData() {
        return LocalDate.now().toString() + "-" + UUID.randomUUID().toString();
    }
}
