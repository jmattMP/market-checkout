package com.jmattMP.market.checkout.domain.tax;

import com.jmattMP.market.checkout.domain.Money;
import com.jmattMP.market.checkout.domain.common.DomainPolicy;
import com.jmattMP.market.checkout.domain.product.ProductType;

@DomainPolicy
public interface TaxPolicy {
    /**
     * calculates tax per product type based on
     * net value
     */
    Tax calculateTax(ProductType productType, Money net);
}