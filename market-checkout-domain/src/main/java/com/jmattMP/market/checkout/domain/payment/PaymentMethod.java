package com.jmattMP.market.checkout.domain.payment;

public enum PaymentMethod {
    CASH, CREDIT_CARD
}
