package core.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class CheckLogging {
    private static Logger logger = LoggerFactory.getLogger(CheckLogging.class);
    //private static Logger logger = LogManager.getLogger(CheckLogging.class);

    public static void loggerCore() {
        logger.trace("A TRACE Message");
        logger.debug("A DEBUG Message");
        logger.info("An INFO Message");
        logger.warn("A WARN Message");
        logger.error("An ERROR Message");
    }
}
