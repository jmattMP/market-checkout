package com.jmattMP.market.checkout.tests.acceptance.checkout;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ScanningProducts {
    /**
     * Scenario: Customer scan 3 products and get count of scanned products.
     */
    @Given("first product is scanned")
    public void first_product_is_scanned() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("second product is scanned")
    public void second_product_is_scanned() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("third product is scanned")
    public void third_product_is_scanned() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("I check the count of products")
    public void i_check_the_count_of_products() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("Market checkout return me {int} products in basket")
    public void market_checkout_return_me_products_in_basket(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    /**
     * Scenario: Customer scan 3 products and get actual price of every product.
     */
    @Given("product is scanned")
    public void product_is_scanned() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("product two is scanned")
    public void product_two_is_scanned() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("product three is scanned")
    public void product_three_is_scanned() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("Market checkout return list of price of every items in basket.")
    public void market_checkout_return_list_of_price_of_every_items_in_basket() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    /**
     * Scenario: Customer scan 3 products and get total price of all scanned products.
     */
    @Given("product is scanned and placed in basket")
    public void product_is_scanned_and_placed_in_basket() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("product number two is scanned")
    public void product_number_two_is_scanned() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("product number three is scanned")
    public void product_number_three_is_scanned() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("market checkout return total price for basket.")
    public void market_checkout_return_total_price_for_basket() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    /**
     * Scenario: Customer scan 3 products, remove one of them and number of product is 2
     */
    @Given("product one is scanned")
    public void product_one_is_scanned() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("product II is scanned")
    public void product_II_is_scanned() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("Product III is scanned")
    public void product_III_is_scanned() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("Product III is removed")
    public void product_III_is_removed() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("number of products is {int}")
    public void number_of_products_is(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }
}
