package com.jmattMP.market.checkout.tests.acceptance.examples;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.assertj.core.api.Assertions.assertThat;

public class Adding {

    private Integer sum;

    @Given("user wants to sum the following numbers:")
    public void user_wants_to_sum_the_following_numbers(io.cucumber.datatable.DataTable dataTable) {
        // Write code here that turns the phrase above into concrete actions
        // For automatic transformation, change DataTable to one of
        // E, List<E>, List<List<E>>, List<Map<K,V>>, Map<K,V> or
        // Map<K, List<V>>. E,K,V must be a String, Integer, Float,
        // Double, Byte, Short, Long, BigInteger or BigDecimal.
        //
        // For other transformations you can register a DataTableType.
        sum = dataTable.asList().stream()
                .mapToInt(num -> Integer.parseInt(num))
                .sum();
        assertThat(sum).isPositive();
    }

    @When("user executes sum function")
    public void user_executes_sum_function() {
        // Write code here that turns the phrase above into concrete actions
        assertThat(sum).isGreaterThan(10);
    }

    @Then("the sum is {int}")
    public void the_sum_is(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        assertThat(sum).isEqualTo(60);
    }
}
