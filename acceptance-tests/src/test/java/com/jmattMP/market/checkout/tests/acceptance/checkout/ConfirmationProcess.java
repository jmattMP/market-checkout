package com.jmattMP.market.checkout.tests.acceptance.checkout;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ConfirmationProcess {

    /**
     * Scenario: Customer get printed receipt
     */
    @Given("customer has paid successfully for order")
    public void customer_has_paid_successfully_for_order() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer choose {string}")
    public void customer_choose(String string) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("receipt is printed")
    public void receipt_is_printed() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    /**
     * Scenario: Customer get receipts via email
     */
    @Given("customer has paid successfully for his order")
    public void customer_has_paid_successfully_for_his_order() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer provide correct email")
    public void customer_provide_correct_email() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("receipt is send via email")
    public void receipt_is_send_via_email() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }
}
