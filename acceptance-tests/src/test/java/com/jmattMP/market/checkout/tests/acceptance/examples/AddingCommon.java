package com.jmattMP.market.checkout.tests.acceptance.examples;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.assertj.core.api.Assertions.assertThat;

public class AddingCommon {
    private Integer sum = 0;

    @Given("first number is {int}")
    public void first_number_is(Integer int1) {
        sum += int1;
    }

    @Given("second number is {int}")
    public void second_number_is(Integer int1) {
        sum += int1;
    }

    @When("user One executes sum function")
    public void user_One_executes_sum_function() {
    }

    @Then("the sum for user One is {int}")
    public void the_sum_for_user_One_is(Integer int1) {
        assertThat(sum).isEqualTo(int1);
    }

}
