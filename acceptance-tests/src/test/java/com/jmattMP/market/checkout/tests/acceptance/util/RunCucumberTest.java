package com.jmattMP.market.checkout.tests.acceptance.util;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = "pretty",
        features = {"src/test/resources/features", "src/test/resources/examples"},
        glue = {"com.jmattMP.market.checkout.tests.acceptance.checkout", "com.jmattMP.market.checkout.tests.acceptance.examples"})
public class RunCucumberTest {
}
