package com.jmattMP.market.checkout.tests.acceptance.checkout;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RebatePolicyProcess {

    /**
     * Scenario Outline: Buy x the same products and get get rebate
     *
     * @param int1 Product code
     * @param int2 count of products
     */
    @Given("the same products {int} are taken {int} times")
    public void the_same_products_are_taken_times(Integer int1, Integer int2) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("order is accepted by customer")
    public void order_is_accepted_by_customer() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("rebate for customer is calculated for {int}")
    public void rebate_for_customer_is_calculated_for(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    /**
     * Scenario Outline: Buy three different product and get the cheapest for free
     *
     * @param int1 product code
     */
    @Given("first product is {int}")
    public void first_product_is(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("second product is {int}")
    public void second_product_is(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("third product is {int}")
    public void third_product_is(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("the customer accept order")
    public void the_customer_accept_order() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("the rebate for customer is calculated for {int}")
    public void the_rebate_for_customer_is_calculated_for(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    /**
     * Scenario Outline: Buy four different products and get 10% of rebate
     *
     * @param int1 product code
     */
    @Given("the first product is {int}")
    public void the_first_product_is(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("the second product is {int}")
    public void the_second_product_is(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("the third product is {int}")
    public void the_third_product_is(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Given("the fourth product is {int}")
    public void the_fourth_product_is(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("this customer accept  the order")
    public void this_customer_accept_the_order() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("this rebate for customer is calculated at {int} in %")
    public void this_rebate_for_customer_is_calculated_at_in(Integer int1) {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }



}
