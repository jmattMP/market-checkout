package com.jmattMP.market.checkout.tests.acceptance.checkout;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class PaymentProcess {

    /**
     * Scenario: Customer pay with cash
     */
    @Given("customer has order with some scanned products")
    public void customer_has_order_with_some_scanned_products() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer choose - pay with cash")
    public void customer_choose_pay_with_cash() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer pay with cash")
    public void customer_pay_with_cash() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("payment is accepted")
    public void payment_is_accepted() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    /**
     * Scenario: Customer pay with credit card with insufficient amount of money
     */
    @Given("customer has order with few scanned products")
    public void customer_has_order_with_few_scanned_products() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer choose - pay with card")
    public void customer_choose_pay_with_card() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer has insufficient amount of money")
    public void customer_has_insufficient_amount_of_money() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("payment is rejected")
    public void payment_is_rejected() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    /**
     * Scenario: Customer pay with credit card with sufficient amount of money
     */
    @Given("customer has completed order")
    public void customer_has_completed_order() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer choose paying with credit card")
    public void customer_choose_paying_with_credit_card() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer has sufficient amount of money")
    public void customer_has_sufficient_amount_of_money() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("payment for order is accepted")
    public void payment_for_order_is_accepted() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    /**
     * Scenario: Customer edit order
     */
    @Given("customer has order with three items")
    public void customer_has_order_with_three_items() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer choose edit order")
    public void customer_choose_edit_order() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer remove one of product")
    public void customer_remove_one_of_product() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("number of item in basket decrease for two items")
    public void number_of_item_in_basket_decrease_for_two_items() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    /**
     * Scenario: Customer cancel order
     */
    @Given("customer has order with some products")
    public void customer_has_order_with_some_products() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer choose cancel order")
    public void customer_choose_cancel_order() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("order is canceled")
    public void order_is_canceled() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    /**
     * Scenario:
     * Market checkout carer check and accept order with regulated products
     */
    @Given("customer has order with some regulated products")
    public void customer_has_order_with_some_regulated_products() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("customer choose to pay with credit card")
    public void customer_choose_to_pay_with_credit_card() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @When("market checkout is blocked and get information that some products are regulated")
    public void market_checkout_is_blocked_and_get_information_that_some_products_are_regulated() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }

    @Then("market checkout carer check if customer is in law and accept order")
    public void market_checkout_carer_check_if_customer_is_in_law_and_accept_order() {
        // Write code here that turns the phrase above into concrete actions
        throw new cucumber.api.PendingException();
    }
}
