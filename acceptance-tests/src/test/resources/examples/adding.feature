Feature: Addition

  # Using Cucumber DataTable to get all inputs - addition of 3 numbers
  Scenario: Sum of multiple numbers
    Given user wants to sum the following numbers:
      | 10 |
      | 20 |
      | 30 |
    When user executes sum function
    Then the sum is 60


Scenario Outline: Sum of two numbers - version 5

    Given first number is <firstNumber>
    And second number is <secondNumber>
    When user One executes sum function
    Then the sum for user One is <result>

    Examples:
      | firstNumber | secondNumber | result |
      | 10          | 20           | 30     |
      | 50          | 60           | 110    |