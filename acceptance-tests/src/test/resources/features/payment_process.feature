Feature: Payment process

Background:
[User Story]
As a customer when all products are scanned I want to pay with cash because its preferable way to pay
As a customer when all products are scanned I want to pay with credit card  because its preferable way to pay
As a customer when all product are scanned I want to edit order
As a customer when all product are scanned I want to cancel order
As a market checkout carer when order contains regulated products (alcohol, tabacco) order
will be blocked until market checkout carer doesn't accept it so that market checkout carer can check if person is in law to buy regulated products


[Assumptions]
All products are scanned, calculated and accepted through customer.

[Furthermore user story]
As a customer when all products are scanned I want to pay partialy with credit card and cash because I have no sufficient amount of cash
As a customer I want to have opportunity to have option "split payment" in order to split with compatriot if we buy together.

Scenario: Customer pay with cash
    Given customer has order with some scanned products
    When customer choose - pay with cash
    And customer pay with cash
    Then payment is accepted
Scenario: Customer pay with credit card with insufficient amount of money
    Given customer has order with few scanned products
    When customer choose - pay with card
    And customer has insufficient amount of money
    Then payment is rejected
Scenario: Customer pay with credit card with sufficient amount of money
    Given customer has completed order
    When customer choose paying with credit card
    And customer has sufficient amount of money
    Then payment for order is accepted
Scenario: Customer edit order
    Given customer has order with three items
    When customer choose edit order
    And customer remove one of product
    Then number of item in basket decrease for two items
Scenario: Customer cancel order
    Given customer has order with some products
    When customer choose cancel order
    Then order is canceled
Scenario: Market checkout carer check and accept order with regulated products
    Given customer has order with some regulated products
    When customer choose to pay with credit card
    And market checkout is blocked and get information that some products are regulated
    Then market checkout carer check if customer is in law and accept order