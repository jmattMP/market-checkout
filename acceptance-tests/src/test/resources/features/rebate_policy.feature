Feature: Calculate rebates according to rebate policy

Background:
[User Story: ....]
As a customer I want to get rebate for product X when I buy x unit of this product
As a customer I want to get rebate for order if I buy products together (e.g X1 + X2 + X3)

[Assumptions]

[Furthermore user story]
As a owner of market-checkout I want to configure rebate policy in order to change offer.
As a customer I want to get rebate when I spend more then XXX amount of money
As a customer I want to get rebate for customer who are longer with us

Scenario Outline: Buy x the same products and get get rebate
    Given the same products <chosenProduct> are taken <unit> times
    When order is accepted by customer
    Then rebate for customer is calculated for <rebate>

    Examples:
      | chosenProduct | unit | rebate |
      | 1              | 3    | -50    |
      | 2              | 2    | -5     |
      | 3              | 4    | -60    |
      | 4              | 2    | -10    |


Scenario Outline: Buy three different product and get the cheapest for free
    Given first product is <firstProduct>
    And second product is <secondProduct>
    And third product is <thirdProduct>
    When the customer accept order
    Then the rebate for customer is calculated for <rebate>

    Examples:
      | firstProduct | secondProduct | thirdProduct| rebate|
      | 1            | 2             | 3           |  -10  |
      | 1            | 2             | 4           |  -10  |
      | 1            | 4             | 3           |  -25  |
      | 4            | 2             | 3           |  -10  |
      | 1            | 2             | 3           |  -10  |

Scenario Outline: Buy four different products and get 10% of rebate
    Given the first product is <firstProduct>
    And  the second product is <secondProduct>
    And the third product is <thirdProduct>
    And the fourth product is <fourthProduct>
    When this customer accept  the order
    Then this rebate for customer is calculated at <rebate> in %

     Examples:
          | firstProduct | secondProduct | thirdProduct| fourthProduct|rebate|
          | 1            | 2             | 3           |      4       |  -10 |
          | 1            | 2             | 3           |      3       |   0  |
          | 1            | 1             | 1           |      1       |   0  |
