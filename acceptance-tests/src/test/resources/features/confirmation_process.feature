Feature: Confirmation payment and order

Background:
[User Story: ....]
As a customer I want to get receipt after successful paym


[Assumptions]
Payment has performed successfully

[Furthermore user story]
As a customer I want to send the receipt via email so that I protect environment

Scenario: Customer get printed receipt
    Given customer has paid successfully for order
    When customer choose "print receipt"
    Then receipt is printed

Scenario: Customer get receipts via email
    Given customer has paid successfully for his order
    When customer choose "send receipt"
    And customer provide correct email
    Then receipt is send via email


