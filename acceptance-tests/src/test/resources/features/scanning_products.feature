Feature: Scanning products

Background:
[User Story]
As a customer I want to scan X number of products and see actual count of products so that I can check everything is scanned correctly
As a customer I want to scan X number of products and see actual price of every product so that I can check if every prices are the same I saw on market place.
As a customer I want to scan X number of products and see actual total price for all scanned products so that I can be sure it's affordable sum for me.
As a customer I want to scan X number of products and remove one of them from basket so that I can be sure if too expensive product could be removed.


[Assumptions]
Market checkout is empty and ready to cooperate, All products are registred in market db.
Assume that products without barcode need to be weight, put into bag and have sticker with actual price, so market-checkout know actual price.
Assume that every scaned product is imedietlly put into market weight(in our dictionery basket) in order to check correctness
On scanning stage regulated products (alcohol, tabacco) are not checked

[Furthermore user story]
As a customer I want to see product expiration date in order to be sure if product is not expired.

Scenario: Customer scan 3 products and get count of scanned products.
    Given first product is scanned
    And second product is scanned
    And third product is scanned
    When I check the count of products
    Then Market checkout return me 3 products in basket
Scenario: Customer scan 3 products and get actual price of every product.
    Given product is scanned
    And product two is scanned
    And product three is scanned
    Then Market checkout return list of price of every items in basket.
Scenario: Customer scan 3 products and get total price of all scanned products.
    Given product is scanned and placed in basket
    And product number two is scanned
    And product number three is scanned
    Then market checkout return total price for basket.
Scenario: Customer scan 3 products, remove one of them and number of product is 2
    Given product one is scanned
    And product II is scanned
    And Product III is scanned
    And Product III is removed
    Then number of products is 2
