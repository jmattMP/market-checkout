package com.jmattMP.market.checkout.service.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class OrderItem {
    private final int orderItemId;
    private final int price; //todo change on Money representation
}
