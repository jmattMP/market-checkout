package com.jmattMP.market.checkout.application.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

/**
 * Data transfer object for representing Product as public API for web services
 */

@AllArgsConstructor
@Getter
public class ProductAPI {
    private long productBarcode;
    private String productName;
    private BigDecimal unitPrice;
    private String producerData;
    private LocalDate expirationDate;
    private String productType;
    private List<String> productTags;
    private String productDetails;
}
