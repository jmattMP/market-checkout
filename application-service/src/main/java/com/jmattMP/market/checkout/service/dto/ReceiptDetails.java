package com.jmattMP.market.checkout.service.dto;

import lombok.Getter;

import java.util.List;

@Getter
public class ReceiptDetails {
    private List<OrderItem> orderItems;
    private int totalPrice;

    public ReceiptDetails(List<OrderItem> orderItems, int totalPrice) {
        this.orderItems = orderItems;
        this.totalPrice = totalPrice;
    }
}
