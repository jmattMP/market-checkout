package com.jmattMP.market.checkout.application.port.out;

import com.jmattMP.market.checkout.application.dto.ProductState;

import java.util.Collection;

public interface ProductRepository {
    Collection<ProductState> getAvailableProductsByNameAndProductType(String name, String productType);

    Collection<ProductState> getAvailableProductsByName(String name);

    Collection<ProductState> getAvailableProductsByProductType(String productType);

    Collection<ProductState> getAllProducts();

    void addNewProduct(ProductState product);

    ProductState getProductByBarcode(long barcode);

    ProductState removeProduct(long productBarcode);
}
