package com.jmattMP.market.checkout.application.mapper;

import com.jmattMP.market.checkout.application.dto.ProductAPI;
import com.jmattMP.market.checkout.application.dto.ProductState;
import com.jmattMP.market.checkout.domain.Money;
import com.jmattMP.market.checkout.domain.product.Product;

import java.util.Collection;

import static java.util.stream.Collectors.toList;


public class ProductMapper {

    //Product <-> ProductState
    public static ProductState productToProductState(Product product) {
        return new ProductState(product.getProductBarcode(),
                product.getProductName(),
                product.getUnitPrice().getCurrentAmountAsBigDecimal(),
                product.getProducerData(),
                product.getExpirationDate(),
                product.getProductType(),
                product.getProductTags(),
                product.getProductDetails());
    }

    public static Product productStateToProduct(ProductState productState) {
        return new Product(productState.getProductBarcode(),
                productState.getProductName(),
                Money.inPLNFrom(productState.getUnitPrice().doubleValue()),
                productState.getProducerData(),
                productState.getExpirationDate(),
                productState.getProductType(),
                productState.getProductTags(),
                productState.getProductDetails());
    }

    public static Collection<ProductState> productsToProductsState(Collection<Product> products) {
        return products.stream().map(ProductMapper::productToProductState).collect(toList());
    }

    public static Collection<Product> productsStateToProducts(Collection<ProductState> productsState) {
        return productsState.stream().map(ProductMapper::productStateToProduct).collect(toList());
    }

    //Product <-> ProductAPI
    public static ProductAPI productToProductApi(Product product) {
        return new ProductAPI(product.getProductBarcode(),
                product.getProductName(),
                product.getUnitPrice().getCurrentAmountAsBigDecimal(),
                product.getProducerData(),
                product.getExpirationDate(),
                product.getProductType(),
                product.getProductTags(),
                product.getProductDetails());
    }

    public static Product productApiToProduct(ProductAPI productApi) {
        return new Product(productApi.getProductBarcode(),
                productApi.getProductName(),
                Money.inPLNFrom(productApi.getUnitPrice().doubleValue()),
                productApi.getProducerData(),
                productApi.getExpirationDate(),
                productApi.getProductType(),
                productApi.getProductTags(),
                productApi.getProductDetails());
    }

    public static Collection<ProductAPI> productsToProductsApi(Collection<Product> products) {
        return products.stream().map(ProductMapper::productToProductApi).collect(toList());
    }

    public static Collection<Product> productsApiToProducts(Collection<ProductAPI> productsApi) {
        return productsApi.stream().map(ProductMapper::productApiToProduct).collect(toList());
    }

    //ProductAPI <-> ProductState
    public static ProductAPI productStateToProductApi(ProductState productState) {
        return new ProductAPI(productState.getProductBarcode(),
                productState.getProductName(),
                productState.getUnitPrice(),
                productState.getProducerData(),
                productState.getExpirationDate(),
                productState.getProductType(),
                productState.getProductTags(),
                productState.getProductDetails());
    }

    public static ProductState productApiToProductState(ProductAPI productApi) {
        return new ProductState(productApi.getProductBarcode(),
                productApi.getProductName(),
                productApi.getUnitPrice(),
                productApi.getProducerData(),
                productApi.getExpirationDate(),
                productApi.getProductType(),
                productApi.getProductTags(),
                productApi.getProductDetails());
    }

    public static Collection<ProductAPI> productsStateToProductsApi(Collection<ProductState> productsState) {
        return productsState.stream().map(ProductMapper::productStateToProductApi).collect(toList());
    }

    public static Collection<ProductState> productsApiToProductsState(Collection<ProductAPI> productsApi) {
        return productsApi.stream().map(ProductMapper::productApiToProductState).collect(toList());
    }


}
