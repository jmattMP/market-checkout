package com.jmattMP.market.checkout.service.dto;

public enum PaymentProgress {
    IN_PROGRESS, ACCEPTED, CANCELED;
}
