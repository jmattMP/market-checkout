package com.jmattMP.market.checkout.application.service;

import com.jmattMP.market.checkout.application.dto.ProductAPI;
import com.jmattMP.market.checkout.application.dto.ProductState;
import com.jmattMP.market.checkout.application.mapper.ProductMapper;
import com.jmattMP.market.checkout.application.port.in.ProductCrud;
import com.jmattMP.market.checkout.application.port.out.ProductRepository;
import lombok.extern.slf4j.Slf4j;

import java.util.Collection;

@Slf4j
public class ProductCrudService implements ProductCrud {

    private final ProductRepository productRepository;

    public ProductCrudService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public Collection<ProductAPI> getAvailableProducts(String name, String productType) {
        log.info("Get available products");
        Collection<ProductState> productStates;
        if (null == name && null == productType) {
            productStates = productRepository.getAllProducts();
        } else if (null == productType) {
            productStates = productRepository.getAvailableProductsByName(name);
        } else if (null == name) {
            productStates = productRepository.getAvailableProductsByProductType(productType);
        } else {
            productStates = productRepository.getAvailableProductsByNameAndProductType(name, productType);
        }
        return ProductMapper.productsStateToProductsApi(productStates);
    }

    @Override
    public void addNewProduct(ProductAPI product) {
        log.info("Add product");
        ProductState newProductState = ProductMapper.productApiToProductState(product);
        productRepository.addNewProduct(newProductState);
    }

    @Override
    public ProductAPI getProductByBarcode(long productBarcode) {
        log.info("Return product with barcode {}", productBarcode);
        return ProductMapper.productStateToProductApi(productRepository.getProductByBarcode(productBarcode));
    }

    @Override
    public ProductAPI removeProduct(long productBarcode) {
        log.info("Removing product by barcode {}", productBarcode);
        return ProductMapper.productStateToProductApi(productRepository.removeProduct(productBarcode));
    }
}
