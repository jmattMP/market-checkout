package com.jmattMP.market.checkout.service.dto;

import com.jmattMP.market.checkout.domain.payment.PaymentMethod;
import lombok.Getter;

import java.util.List;

@Getter
public class PaymentMethodResponse {
    private List<PaymentMethod> paymentMethods;


    public PaymentMethodResponse(List<PaymentMethod> paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    //TODO consider not transfer domain element but maybe special service dto for that (mapstruct??)
    public List<PaymentMethod> getPaymentMethods() {
        return paymentMethods;
    }
}
