package com.jmattMP.market.checkout.application.service;

import com.jmattMP.market.checkout.application.port.in.OrderProcessingUseCase;
import com.jmattMP.market.checkout.domain.order.Order;
import com.jmattMP.market.checkout.domain.order.OrderRepository;
import com.jmattMP.market.checkout.service.dto.OrderItem;
import com.jmattMP.market.checkout.service.dto.OrderRepresentation;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OrderProcessingService implements OrderProcessingUseCase {


    private final OrderRepository orderRepository;


    @Override
    public OrderRepresentation createNewOrder() {
        Order order = orderRepository.createOrder(Order.createNewOrder());
        return null;
    }

    @Override
    public OrderRepresentation cancelOrder() {
        return null;
    }

    @Override
    public OrderRepresentation submitOrder() {
        return null;
    }

    @Override
    public OrderRepresentation getCurrentOrder() {
        return null;
    }

    @Override
    public OrderItem scanProduct() {
        return null;
    }

    @Override
    public OrderRepresentation addProduct() {
        return null;
    }

    @Override
    public OrderRepresentation removeProduct() {
        return null;
    }
}
