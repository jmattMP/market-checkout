package com.jmattMP.market.checkout.application.port.in;

import com.jmattMP.market.checkout.service.dto.OrderItem;
import com.jmattMP.market.checkout.service.dto.OrderRepresentation;

public interface OrderProcessingUseCase {

    OrderRepresentation createNewOrder();

    OrderRepresentation cancelOrder();

    OrderRepresentation submitOrder();

    OrderRepresentation getCurrentOrder();

    OrderItem scanProduct();

    OrderRepresentation addProduct();

    OrderRepresentation removeProduct();

}
