package com.jmattMP.market.checkout.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@AllArgsConstructor
@Getter
public class OrderRepresentation {

    private final int orderRepresentationId;
    private final List<OrderItem> orderItems;
    private final int totalPrice; //todo change on money representation
}
