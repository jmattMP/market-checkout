package com.jmattMP.market.checkout.service.dto;

import lombok.Getter;

@Getter
public class RebateDetails {
    private int totalOrderPrice;
    private int totalRebateAmount;

    public RebateDetails(int totalOrderPrice, int totalRebateAmount) {
        this.totalOrderPrice = totalOrderPrice;
        this.totalRebateAmount = totalRebateAmount;
    }
}
