package com.jmattMP.market.checkout.application.port.in;

import com.jmattMP.market.checkout.application.dto.ProductAPI;

import java.util.Collection;

public interface ProductCrud {
    Collection<ProductAPI> getAvailableProducts(String name, String productType);

    void addNewProduct(ProductAPI product);

    ProductAPI getProductByBarcode(long productBarcode);

    ProductAPI removeProduct(long productBarcode);
}
