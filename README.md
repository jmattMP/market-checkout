## Business requirements
- Design and implement market checkout component with readable API that calculates the total price of a number of items.
- Checkout mechanism can scan items and return actual price (is stateful).
- Our goods are priced individually.
- Some items are multi-priced: buy N of them, and they’ll cost you Y cents.

| Item | Price | Unit | Special price |
| ---- | ----- | ---- | ------------- |
| A | 40 | 3 | for 70 |
| B | 10 | 2 | for 15 |
| C | 30 | 4 | for 60 |
| D | 25 | 2 | for 40 |

- Client receives receipt containing list of all products with corresponding prices after payment.
- Some items are cheaper when bought together - buy item X with item Y and save Z cents.

| Items combination | Price | Promotion description |
| ----------------- | ----------------- | --------------------------------------------- |
| A + B + C | 70 instead of 80 | Buy 3 products, get the cheapest for free |
| A + B + C + D | 94,5 instead of 105 | Buy four different products and get 10% cheaper |

## Technical requirements
- The output of this task should be a project with buildable production ready service, that
can be executed independently of the source code.
- Project should include all tests, including unit, integration and acceptance tests.
- The service should expose REST api, without any UI.
- You can use gradle, maven or any other building tool.
- It ought to be written in Java 8.
- If requested, please use Spring or any other framework, otherwise, the choice is yours.
- Please include a short description of how to build and execute your project in e.g.
README.md file. It it's up to you to make your own assumptions about the code and
assignment. Each assumption should be captured as part of README.md file, so that we can review them before reading a code. 
There are no good and bad answers here. You can put there any other information that you would like to share with us like important classes, design overview, the rationale for your choices etc.

## Further extensions
Some extension could be applicable in the future, and are divided into business as well technical change requests:

### business future/potential change requests
- Limitation regarding law e.g minimum Age, religion, only for citizens.
- Additional Discount of 10% should be added if total order price (including other discounts/special prices) exceeds 600
- Market can add any type of item or discount at any point in time.
- Market stores data about quantity of all items sold in all its checkouts and all applied discounts.
- Counting/Observing inventory - if number of specific product is less then 30% (30%<) then event `supplyProductEvent` is fired.

### technical future/potential change requests
- Perform a simple simulation of a market with 3 Checkouts and a single queue of 10 random baskets of items.
- Audit feature
- mail instead of paper receipt.

### assumptions
- Market checkout is thread safe because every customer has own market-checkout and they interact only via DB (number of products).
- Every product has own unique ID, no possibility to find on market place two "the same" (regarding to ID) product on market store.
